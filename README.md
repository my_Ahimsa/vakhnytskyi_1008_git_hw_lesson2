# Vakhnytskyi_1008_git_hw_Lesson2
> Завдання 1
>> ~~Створіть проект на Gitlab і додайте файли до репозиторію.~~

> Завдання 2
>> ~~Створіть проект на GitHub і додайте файли до репозиторію.~~
>> посилання на створений проектна GitHub:
>> https://github.com/myAhimsa/Task2_Lesson2_git_CB

> Завдання 3
>> ~~Створіть проект додайте 3 файли та створіть коміт. Створіть нову гілку та додайте ще 5 файлів. Виконайте об’єднання гілок. Після виконання злиття гілок зайву видаліть.~~
<details><summary>Лог з GitBash з виконання завдання №3:</summary>
Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 cPythonCyberBionic Systematics PythonDeveloperGiTgit_2
$ cd Vakhnytskyi hw Lesson2

Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 cPythonCyberBionic Systematics PythonDeveloperGiTgit_2Vakhnytskyi hw Lesson2
$ git init
Initialized empty Git repository in CPythonCyberBionic Systematics PythonDeveloperGiTgit_2Vakhnytskyi hw Lesson2.git

Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 cPythonCyberBionic Systematics PythonDeveloperGiTgit_2Vakhnytskyi hw Lesson2 (master)
$ touch .gitingore

Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 cPythonCyberBionic Systematics PythonDeveloperGiTgit_2Vakhnytskyi hw Lesson2 (master)
$ nano .gitingore

Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 cPythonCyberBionic Systematics PythonDeveloperGiTgit_2Vakhnytskyi hw Lesson2 (master)
$ git commit -m added and fulfilled .gitignore file

Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 cPythonCyberBionic Systematics PythonDeveloperGiTgit_2Vakhnytskyi hw Lesson2 (master)
$ touch new_file.txt

Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 cPythonCyberBionic Systematics PythonDeveloperGiTgit_2Vakhnytskyi hw Lesson2 (master)
$ nano new_file.txt

Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 cPythonCyberBionic Systematics PythonDeveloperGiTgit_2Vakhnytskyi hw Lesson2 (master)
$ touch summation.py

Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 cPythonCyberBionic Systematics PythonDeveloperGiTgit_2Vakhnytskyi hw Lesson2 (master)
$ nano summation.py

Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 cPythonCyberBionic Systematics PythonDeveloperGiTgit_2Vakhnytskyi hw Lesson2 (master)
$ git add summation.py new_file.txt
warning in the working copy of '.gitingore', LF will be replaced by CRLF the next time Git touches it

Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 cPythonCyberBionic Systematics PythonDeveloperGiTgit_2Vakhnytskyi hw Lesson2 (master)
$ git commit -m added and fulfilled two files
Merge made by the 'ort' strategy.
 new_added_1.png  0
 new_added_2.png  0
 new_added_3.png  0
 new_added_4.png  0
 new_added_5.png  0
 5 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 new_added_1.png
 create mode 100644 new_added_2.png
 create mode 100644 new_added_3.png
 create mode 100644 new_added_4.png
 create mode 100644 new_added_5.png

Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 cPythonCyberBionic Systematics PythonDeveloperGiTgit_2Vakhnytskyi hw Lesson2 (master)
$ git branch -d v001
Deleted branch v001 (was 081c229).

Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 cPythonCyberBionic Systematics PythonDeveloperGiTgit_2Vakhnytskyi hw Lesson2 (master)
$

**Дії що виконувались в гілці v001, а саме: створення та редагування через консоль (Створіть нову гілку та додайте ще 5 файлів) не відображені, оскільки видалились з GitBash після видалення гілки((( але всі файли що були створені - успішно вмерджені в гілку master**
</details>


> Завдання 4
>> ~~Надішліть репозиторій на віддалений сервер.~~
>>  <details><summary>Лог з GitBash з виконання завдання 4</summary>
>> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2 (master)
>>
>> $ git remote add origin https://gitlab.com/my_Ahimsa/vakhnytskyi_1008_git_hw_lesson2.git
>> 
>> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2 (master)
>> $ git push
>> fatal: The current branch master has no upstream branch.
>> To push the current branch and set the remote as upstream, use
>> 
>>     git push --set-upstream origin master
>> 
>> To have this happen automatically for branches without a tracking
>> upstream, see 'push.autoSetupRemote' in 'git help config'.
>> 
>> 
>> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2 (master)
>> Enumerating objects: 12, done.in master
>> Counting objects: 100% (12/12), done.
>> Delta compression using up to 8 threads
>> Compressing objects: 100% (10/10), done.
>> Writing objects: 100% (12/12), 3.45 KiB | 3.45 MiB/s, done.
>> Total 12 (delta 3), reused 0 (delta 0), pack-reused 0
>> remote:
>> remote: To create a merge request for master, visit:
>> remote:   https://gitlab.com/my_Ahimsa/vakhnytskyi_1008_git_hw_lesson2/-/merge_requests/new?merge_request%5Bsource_branch%5D=master
>> remote:
>> To https://gitlab.com/my_Ahimsa/vakhnytskyi_1008_git_hw_lesson2.git
>>  * [new branch]      master -> master
>> branch 'master' set up to track 'origin/master'.
>> 
>> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2 (master)
>> $
>> 
>> </details>




> Завдання 5
>> ~~Додайте ще гілки dev та test на віддаленому репозиторії й додайте у кожну по 5 файлів. Виконайте коміти файлів. Відобразіть усі гілки на екрані та усі коміти у гілках.~~
>>
>> **Project->Code->Repository graph** - перейшовши - маємо графічне відображення проєкту, з усіма гілками і коммітами.


> Завдання 6
>> ~~Виконайте клонування репозиторія з віддаленного серверу на свій комп’ютер.~~
>>
>><details><summary>Лог клонування репозиторію</summary>
>>Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2 (master)
>> $ git clone https://gitlab.com/my_Ahimsa/vakhnytskyi_1008_git_hw_lesson2.git
>> Cloning into 'vakhnytskyi_1008_git_hw_lesson2'...
>> remote: Enumerating objects: 63, done.
>> remote: Counting objects: 100% (24/24), done.
>> remote: Compressing objects: 100% (23/23), done.
>> remote: Total 63 (delta 11), reused 0 (delta 0), pack-reused 39
>> Receiving objects: 100% (63/63), 15.17 KiB | 1.38 MiB/s, done.
>> Resolving deltas: 100% (24/24), done.
>> 
>> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2 (master)
>> $
>> 
>> </details>


> Завдання 7
>> ~~Створіть нові версії файлів. Створіть коміти. Надішліть зміни на віддалений сервер. Відобразіть усі коміти у гілках.~~
> <details><summary>Лог Завдання 7</summary>
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2 (master)
> $ cd vakhnytskyi_1008_git_hw_lesson2/
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (main)
> $ git checkout
> HEAD            main            origin/HEAD     origin/main     origin/test
> dev             master          origin/dev      origin/master   test
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (main)
> $ git checkout
> HEAD            main            origin/HEAD     origin/main     origin/test
> dev             master          origin/dev      origin/master   test
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (main)
> $ git checkout test
> Switched to a new branch 'test'
> branch 'test' set up to track 'origin/test'.
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (test)
> $ git add .
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (test)
> $ git status
> On branch test
> Your branch is up to date with 'origin/test'.
> 
> Changes to be committed:
>   (use "git restore --staged <file>..." to unstage)
>         modified:   Hello.py
>         modified:   New_file.txt
>         modified:   new_test_file_1.png
>         modified:   new_test_file_2.png
>         modified:   new_test_file_3.png
>         modified:   new_test_file_4.png
>         modified:   new_test_file_5.png
> 
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (test)
> $ git commit -m "All files exept README.md were modifyed as required in Task 7"
> [test e2c1a48] All files exept README.md were modifyed as required in Task 7
>  7 files changed, 5 insertions(+), 2 deletions(-)
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (test)
> $ git checkout dev
> Switched to a new branch 'dev'
> branch 'dev' set up to track 'origin/dev'.
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (dev)
> $ git add .
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (dev)
> $ git commit -m "All files were modifyed, so we expect the colisin now)))"
> [dev b273585] All files were modifyed, so we expect the colisin now)))
>  7 files changed, 15 insertions(+), 3 deletions(-)
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (dev)
> $ git checkout
> HEAD            main            origin/HEAD     origin/main     origin/test
> dev             master          origin/dev      origin/master   test
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (dev)
> $ git checkout master
> Switched to a new branch 'master'
> branch 'master' set up to track 'origin/master'.
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (master)
> $ git add .
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (master)
> $ git commit -m "fulfilled pictures with, and minor changes to side files"
> [master 36c45a7] fulfilled pictures with, and minor changes to side files
>  7 files changed, 6 insertions(+)
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (master)
> $ git push --set-upstream origin
> HEAD            main            origin/HEAD     origin/main     origin/test
> dev             master          origin/dev      origin/master   test
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (master)
> $ git push --set-upstream origin dev test master
> Enumerating objects: 51, done.
> Counting objects: 100% (36/36), done.
> Delta compression using up to 8 threads
> Compressing objects: 100% (22/22), done.
> Writing objects: 100% (27/27), 476.25 KiB | 18.32 MiB/s, done.
> Total 27 (delta 4), reused 0 (delta 0), pack-reused 0
> remote:
> remote: To create a merge request for dev, visit:
> remote:   https://gitlab.com/my_Ahimsa/vakhnytskyi_1008_git_hw_lesson2/-/merge_requests/new?merge_request%5Bsource_branch%5D=dev
> remote:
> remote:
> remote: To create a merge request for master, visit:
> remote:   https://gitlab.com/my_Ahimsa/vakhnytskyi_1008_git_hw_lesson2/-/merge_requests/new?merge_request%5Bsource_branch%5D=master
> remote:
> remote:
> remote: To create a merge request for test, visit:
> remote:   https://gitlab.com/my_Ahimsa/vakhnytskyi_1008_git_hw_lesson2/-/merge_requests/new?merge_request%5Bsource_branch%5D=test
> remote:
> To https://gitlab.com/my_Ahimsa/vakhnytskyi_1008_git_hw_lesson2.git
>    3d4b0b9..b273585  dev -> dev
>    d4e18c3..36c45a7  master -> master
>    b44f3d0..e2c1a48  test -> test
> branch 'dev' set up to track 'origin/dev'.
> branch 'master' set up to track 'origin/master'.
> branch 'test' set up to track 'origin/test'.
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (master)
> $
> 
> </details>


> Завдання 8
>> ~~У віддаленому репозиторії зробіть злиття гілок dev/test та синхронізуйте зміни з локальним репозиторіем.~~
> <details><summary>Лог синхронізації локалки з віддаленим репозиторієм</summary>
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (master)
> $ git pull
> remote: Enumerating objects: 23, done.
> remote: Counting objects: 100% (22/22), done.
> remote: Compressing objects: 100% (11/11), done.
> remote: Total 11 (delta 6), reused 0 (delta 0), pack-reused 0
> Unpacking objects: 100% (11/11), 2.87 KiB | 21.00 KiB/s, done.
> From https://gitlab.com/my_Ahimsa/vakhnytskyi_1008_git_hw_lesson2
>    7c1c3ed..a5f6355  main       -> origin/main
>    e2c1a48..136c46c  test       -> origin/test
> Already up to date.
> 
> Dmitriy.Vakhnytskyi@NB-DVAKHNYTSKYI MINGW64 /c/Python/CyberBionic Systematics PythonDeveloper/GiT/git_2/Vakhnytskyi hw Lesson2/vakhnytskyi_1008_git_hw_lesson2 (master)
> $
> 
> </details>


> Завдання 9
>> Створіть новий репозиторій з модифікатором доступу private. Надайте хоча б 5-м колегам доступ до нього. Перейшовши за посиланням кожний повинен: 
• клонувати проект на ПК;
• створити нову гілку;
• додати декілька файлів;
• надіслати зміни на віддалений сервер;
• зробити code-review.

>> Project ID: 50511723
>> 
>>link to project https://gitlab.com/my_Ahimsa/radiokaruna
>>
>> group of project curently 5 person: https://gitlab.com/my_Ahimsa/radiokaruna/-/project_members
>>
>> Low activity on project...


<details><summary>Click to expand</summary>

cd existing_repo

git remote add origin
https://gitlab.com/my_Ahimsa/vakhnytskyi_1008_git_hw_lesson2.git

git branch -M main

git push -uf origin main
</details>

